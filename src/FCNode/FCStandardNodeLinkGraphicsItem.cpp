﻿#include "FCStandardNodeLinkGraphicsItem.h"


FCStandardNodeLinkGraphicsItem::FCStandardNodeLinkGraphicsItem(QGraphicsItem *p)
    : FCAbstractNodeLinkGraphicsItem(p)
{
}


FCStandardNodeLinkGraphicsItem::FCStandardNodeLinkGraphicsItem(FCAbstractNodeGraphicsItem *from,
    FCNodeLinkPoint pl,
    QGraphicsItem *p)
    : FCAbstractNodeLinkGraphicsItem(from, pl, p)
{
}
