﻿#ifndef FCSTANDARDNODELINKGRAPHICSITEM_H
#define FCSTANDARDNODELINKGRAPHICSITEM_H
#include "FCAbstractNodeLinkGraphicsItem.h"
#include <QtCore/qglobal.h>

class FCNODE_API FCStandardNodeLinkGraphicsItem : public FCAbstractNodeLinkGraphicsItem
{
public:
    FCStandardNodeLinkGraphicsItem(QGraphicsItem *p = nullptr);
    FCStandardNodeLinkGraphicsItem(FCAbstractNodeGraphicsItem *from, FCNodeLinkPoint pl, QGraphicsItem *p = nullptr);
};

#endif // FCSTANDARDNODELINKGRAPHICSITEM_H
