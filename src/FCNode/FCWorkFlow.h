﻿#ifndef FCWORKFLOW_H
#define FCWORKFLOW_H
#include <QtCore/qglobal.h>
#include <memory>
#include <QObject>
#include "FCNodeGlobal.h"
#include "FCAbstractNodeFactory.h"
FC_IMPL_FORWARD_DECL(FCWorkFlow)

/**
 * @brief 基本的工作流,这个也是总工厂，汇总了所有插件的工厂
 */
class FCNODE_API FCWorkFlow : public QObject
{
    Q_OBJECT
    FC_IMPL(FCWorkFlow)
public:
    typedef std::shared_ptr<FCAbstractNode> FCAbstractNodeSmtPtr;
public:
    FCWorkFlow(QObject *p = nullptr);
    ~FCWorkFlow();
    //工厂相关
    void registFactory(FCAbstractNodeFactory *factory);
    void registFactorys(const QList<FCAbstractNodeFactory *> factorys);

    //创建节点，会触发信号nodeCreated,FCWorkFlow保留节点的内存管理权
    FCAbstractNodeSmtPtr createNode(const FCNodeMetaData& md);

signals:

    /**
     * @brief 节点创建产生的信号
     * @param node
     */
    void nodeCreated(FCAbstractNodeSmtPtr node);
};

#endif // FCWORKFLOW_H
