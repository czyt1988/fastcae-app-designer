﻿#ifndef FCABSTRACTNODEFACTORY_H
#define FCABSTRACTNODEFACTORY_H
#include "FCNodeGlobal.h"
#include <QtCore/qglobal.h>
#include <QObject>
#include "FCAbstractNode.h"

/**
 * @brief FCAbstractNode的工厂基类，所有自定义的node集合最后都需要提供一个工厂
 *
 * 工厂将通过@sa FCNodeMetaData 来创建一个FCAbstractNode,FCAbstractNode可以生成FCAbstractNodeGraphicsItem实现前端的渲染，
 * 因此，任何节点都需要实现一个FCAbstractNode和一个FCAbstractNodeGraphicsItem，一个实现逻辑节点的描述，
 * 一个实现前端的渲染,另外FCAbstractNodeGraphicsItem可以生成FCAbstractNodeWidget，用于设置FCAbstractNodeGraphicsItem
 */
class FCNODE_API FCAbstractNodeFactory : public QObject
{
public:
    FCAbstractNodeFactory(QObject *p = nullptr);
    virtual ~FCAbstractNodeFactory();
    //工厂名称
    virtual QString factoryName() const = 0;

    //工厂函数，创建一个FCAbstractNode，工厂不持有FCAbstractNode的管理权
    virtual FCAbstractNode *create(const FCNodeMetaData& meta) = 0;

    //获取所有注册的Prototypes
    virtual QStringList getPrototypes() const = 0;

    //获取所有类型的元数据
    virtual QList<FCNodeMetaData> getNodesMetaData() const = 0;
};

#endif // FCABSTRACTNODEFACTORY_H
