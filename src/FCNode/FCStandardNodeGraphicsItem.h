#ifndef FCSTANDARDNODEGRAPHICSITEM_H
#define FCSTANDARDNODEGRAPHICSITEM_H
#include "FCNodeGlobal.h"
#include "FCAbstractNodeGraphicsItem.h"
FC_IMPL_FORWARD_DECL(FCStandardNodeGraphicsItem)
/**
 * @brief 标准的GraphicsItem
 */
class FCNODE_API FCStandardNodeGraphicsItem : public FCAbstractNodeGraphicsItem
{
    FC_IMPL(FCStandardNodeGraphicsItem)
public:
    enum { Type = FastCAE::GraphicsStandardNodeItem };
    int type() const
    {
        return (Type);
    }
    FCStandardNodeGraphicsItem(FCAbstractNode *n, QGraphicsItem *p = nullptr);
    ~FCStandardNodeGraphicsItem();
    //计算BoundingRect
    virtual void calcBoundingRect();
public:
    //绘图
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    //绘图相关
    QRectF boundingRect() const override;
protected:
    //添加事件处理
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant& value);
};

#endif // FCSTANDARDNODEGRAPHICSITEM_H
