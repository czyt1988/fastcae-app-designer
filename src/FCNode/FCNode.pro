QT       += core gui xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib
DEFINES += FCNODE_BUILDLIB
CONFIG		+=  c++11
CONFIG		+=  qt
include($$PWD/../common.pri)
include($$PWD/../function.pri)
TARGET = $$saLibNameMake(FCNode)
# 通用的设置
$$commonProLibSet($${TARGET})

# 在lib文件夹下编译完后，把dll文件拷贝到bin目录下
$$saCopyLibToBin($${TARGET})

# 依赖
include($${FC_SRC_DIR}/FCPlugin/FCPlugin.pri)

HEADERS += \
    FCAbstractNode.h \
    FCAbstractNodeFactory.h \
    FCAbstractNodeGraphicsItem.h \
    FCAbstractNodeLinkGraphicsItem.h \
    FCAbstractNodePlugin.h \
    FCAbstractNodeWidget.h \
    FCDataItem.h \
    FCDataPackage.h \
    FCNodeGlobal.h \
    FCNodeGraphicsScene.h \
    FCNodeGraphicsView.h \
    FCNodeLinkPoint.h \
    FCNodeMetaData.h \
    FCNodePalette.h \
    FCStandardNodeLinkGraphicsItem.h \
    FCWorkFlow.h \
    FCStandardNodeGraphicsItem.h


SOURCES += \
    FCAbstractNode.cpp \
    FCAbstractNodeFactory.cpp \
    FCAbstractNodeGraphicsItem.cpp \
    FCAbstractNodeLinkGraphicsItem.cpp \
    FCAbstractNodePlugin.cpp \
    FCAbstractNodeWidget.cpp \
    FCDataItem.cpp \
    FCDataPackage.cpp \
    FCNodeGraphicsScene.cpp \
    FCNodeGraphicsView.cpp \
    FCNodeLinkPoint.cpp \
    FCNodeMetaData.cpp \
    FCNodePalette.cpp \
    FCStandardNodeLinkGraphicsItem.cpp \
    FCWorkFlow.cpp \
    FCStandardNodeGraphicsItem.cpp



