﻿#ifndef FCABSTRACTNODEWIDGET_H
#define FCABSTRACTNODEWIDGET_H
#include <QWidget>
#include "FCNodeGlobal.h"
#include "FCAbstractNode.h"
class FCAbstractNodeGraphicsItem;

FC_IMPL_FORWARD_DECL(FCAbstractNodeWidget)

/**
 * @brief FCNodeItem都可返回一个FCNodeWidget，用于设置node属性
 *
 * FCNodeWidget是一个空的窗口，可以通过@sa setWidget 函数设置窗口
 */
class FCNODE_API FCAbstractNodeWidget : public QWidget
{
    //Q_OBJECT
    FC_IMPL(FCAbstractNodeWidget)
public:
    explicit FCAbstractNodeWidget(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    explicit FCAbstractNodeWidget(FCAbstractNodeGraphicsItem *item, QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~FCAbstractNodeWidget();

    //设置节点item
    void setNodeItem(FCAbstractNodeGraphicsItem *item);

    //获取保存的节点item指针
    FCAbstractNodeGraphicsItem *getNodeItem() const;

    //获取节点
    FCAbstractNode *getNode() const;
};

#endif // FCABSTRACTNODEWIDGET_H
