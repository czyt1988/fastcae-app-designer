﻿#ifndef FCTESTNODE_H
#define FCTESTNODE_H

#include <QtCore/qglobal.h>
#include "FCUtilNodePluginGlobal.h"
#include "FCAbstractNode.h"
class FCTestNode : public FCAbstractNode
{
public:
    FCTestNode();
    //获取所有输入的参数名
    virtual QList<QString> getInputKeys() const;

    //获取所有输出的参数名
    virtual QList<QString> getOutputKeys() const;

    //节点对应的item显示接口，所有node都需要提供一个供前端的显示接口
    virtual FCAbstractNodeGraphicsItem *createGraphicsItem();

    //输出参数
    virtual FCDataPackage output(const QString& ouputkey);

private:
    QList<QString> m_input;
    QList<QString> m_output;
};

#endif // FCTESTNODE_H
