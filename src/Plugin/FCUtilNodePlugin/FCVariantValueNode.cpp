﻿#include "FCVariantValueNode.h"
#include "FCVariantValueNodeGraphicsItem.h"
#include <QObject>
#include "FCNodeLinkPoint.h"
#define RES_VARIANT    ":/icon/icon/variant.svg"

class FCVariantValueNodePrivate
{
    FC_IMPL_PUBLIC(FCVariantValueNode)
public:
    FCVariantValueNodePrivate(FCVariantValueNode *p);
    QList<QString> _output;
};

FCVariantValueNodePrivate::FCVariantValueNodePrivate(FCVariantValueNode *p)
    : q_ptr(p)
{
    _output << "out";
}


FCVariantValueNode::FCVariantValueNode() : FCAbstractNode()
    , d_ptr(new FCVariantValueNodePrivate(this))
{
    metaData().setIcon(QIcon(RES_VARIANT));
    metaData().setNodePrototype("FC.Util.Const");
    metaData().setNodeName(QObject::tr("variant"));
    metaData().setGroup(QObject::tr("common"));
}


FCVariantValueNode::~FCVariantValueNode()
{
}


QList<QString> FCVariantValueNode::getInputKeys() const
{
    return (QList<QString>());
}


QList<QString> FCVariantValueNode::getOutputKeys() const
{
    return (d_ptr->_output);
}


FCAbstractNodeGraphicsItem *FCVariantValueNode::createGraphicsItem()
{
    return (new FCVariantValueNodeGraphicsItem(this));
}


FCDataPackage FCVariantValueNode::output(const QString& ouputkey)
{
    return (FCDataPackage());
}
