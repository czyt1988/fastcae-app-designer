﻿#include "FCVariantValueNodeWidget.h"
#include "ui_FCVariantValueNodeWidget.h"

FCVariantValueNodeWidget::FCVariantValueNodeWidget(FCAbstractNodeGraphicsItem *item, QWidget *parent) :
    FCAbstractNodeWidget(item, parent),
    ui(new Ui::FCVariantValueNodeWidget)
{
    ui->setupUi(this);
}


FCVariantValueNodeWidget::~FCVariantValueNodeWidget()
{
    delete ui;
}


void FCVariantValueNodeWidget::apply()
{
    QString name = ui->lineEditName->text();
    QString vstr = ui->lineEditValueText->text();
    QString typestr = ui->comboBoxType->currentText();

    if ("string" == typestr) {
        typestr = "QString";
    }else if ("point" == typestr) {
        typestr = "QPoint";
    }else if ("pointF" == typestr) {
        typestr = "QPointF";
    }else if ("time" == typestr) {
        typestr = "QTime";
    }else if ("datetime" == typestr) {
        typestr = "QDateTime";
    }else if ("size" == typestr) {
        typestr = "QSize";
    }else if ("color" == typestr) {
        typestr = "QColor";
    }
    QVariant v = FCDataPackage::stringToVariant(vstr, typestr);

    qDebug()	<< "typestr:"<<typestr
            << "\nvstr:"<<vstr;
    if (!v.isValid()) {
        ui->labelInfo->setText(tr("variant is invalid!"));
        return;
    }
    m_data.setValueItem("value", FCDataItem(v, name));
    ui->labelInfo->setText(tr(""));
    FCAbstractNode *n = getNode();

    if (n) {
        n->input("_value", m_data.copy());
    }
}


void FCVariantValueNodeWidget::on_comboBoxType_currentIndexChanged(const QString& arg1)
{
    Q_UNUSED(arg1);
    apply();
}


void FCVariantValueNodeWidget::on_lineEditValueText_editingFinished()
{
    apply();
}
