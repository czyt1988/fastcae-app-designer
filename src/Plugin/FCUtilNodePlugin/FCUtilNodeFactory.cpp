﻿#include "FCUtilNodeFactory.h"
#include "FCVariantValueNode.h"
#include "FCTestNode.h"
FCUtilNodeFactory::FCUtilNodeFactory(QObject *p) : FCAbstractNodeFactory(p)
{
    createMetaData();
}


QString FCUtilNodeFactory::factoryName() const
{
    return ("FC.Util");
}


FCAbstractNode *FCUtilNodeFactory::create(const FCNodeMetaData& meta)
{
    FpCreate fp = m_prototypeTpfp.value(meta, nullptr);

    if (fp) {
        return (fp());
    }
    return (nullptr);
}


QStringList FCUtilNodeFactory::getPrototypes() const
{
    QStringList res;

    res.reserve(m_prototypeTpfp.size());
    for (auto i = m_prototypeTpfp.begin(); i != m_prototypeTpfp.end(); ++i)
    {
        res.append(i.key().getNodePrototype());
    }
    return (res);
}


QList<FCNodeMetaData> FCUtilNodeFactory::getNodesMetaData() const
{
    return (m_prototypeTpfp.keys());
}


void FCUtilNodeFactory::createMetaData()
{
    FpCreate fp;

    //FCConstValueNodeGraphicsItem
    fp = []()->FCAbstractNode *{
            return (new FCVariantValueNode());
        };
    m_prototypeTpfp[saveGetMetaType(fp())] = fp;
    //FCTestNodeGraphicsItem
    fp = []()->FCAbstractNode *{
            return (new FCTestNode());
        };
    m_prototypeTpfp[saveGetMetaType(fp())] = fp;
}


/**
 * @brief 这个主要是为了获取到FCAbstractNodeGraphicsItem对应的metadata，获取完metadata后把内存销毁
 * @param t
 * @return
 */
FCNodeMetaData FCUtilNodeFactory::saveGetMetaType(FCAbstractNode *t)
{
    FCNodeMetaData meta = t->metaData();

    delete t;
    return (meta);
}
