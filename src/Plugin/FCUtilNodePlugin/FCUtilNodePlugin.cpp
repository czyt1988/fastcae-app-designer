﻿#include "FCUtilNodePlugin.h"
#include "FCUtilNodeFactory.h"
#include <QObject>

FCAbstractPlugin *plugin_create()
{
    return (new FCUtilNodePlugin());
}


void plugin_destory(FCAbstractPlugin *p)
{
    delete p;
}


FCUtilNodePlugin::FCUtilNodePlugin()
{
}


FCUtilNodePlugin::~FCUtilNodePlugin()
{
}


QString FCUtilNodePlugin::getIID() const
{
    return ("FC.FCUtilNodePlugin");
}


QString FCUtilNodePlugin::getName() const
{
    return ("FC Util Nodes");
}


QString FCUtilNodePlugin::getVersion() const
{
    return ("version 0.1.1");
}


QString FCUtilNodePlugin::getDescription() const
{
    return ("FastCAE Util Nodes");
}


FCAbstractNodeFactory *FCUtilNodePlugin::createNodeFactory()
{
    return (new FCUtilNodeFactory());
}


void FCUtilNodePlugin::destoryNodeFactory(FCAbstractNodeFactory *p)
{
    if (p) {
        p->deleteLater();
    }
}
