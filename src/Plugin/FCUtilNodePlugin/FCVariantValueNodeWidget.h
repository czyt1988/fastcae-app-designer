﻿#ifndef FCVARIANTVALUENODEWIDGET_H
#define FCVARIANTVALUENODEWIDGET_H
#include "FCUtilNodePluginGlobal.h"
#include "FCAbstractNodeWidget.h"
#include <QWidget>

namespace Ui {
class FCVariantValueNodeWidget;
}

/**
 * @brief FCConstValueNode节点对应的窗口
 */
class FCUTILNODEPLUGIN_API FCVariantValueNodeWidget : public FCAbstractNodeWidget
{
    Q_OBJECT

public:
    explicit FCVariantValueNodeWidget(FCAbstractNodeGraphicsItem *item, QWidget *parent = nullptr);
    ~FCVariantValueNodeWidget();
    //应用设置
    void apply();

private slots:
    void on_comboBoxType_currentIndexChanged(const QString& arg1);

    void on_lineEditValueText_editingFinished();

private:
    Ui::FCVariantValueNodeWidget *ui;
    FCDataPackage m_data;
};

#endif // FCVARIANTVALUENODEWIDGET_H
