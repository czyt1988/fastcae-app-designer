﻿#include "FCTestNode.h"
#include "FCTestNodeGraphicsItem.h"
#include "FCNodeLinkPoint.h"
#define RES_ICON    ":/icon/icon/test.svg"
FCTestNode::FCTestNode() : FCAbstractNode()
{
    metaData().setIcon(QIcon(RES_ICON));
    metaData().setNodePrototype("FC.Test.Const");
    metaData().setNodeName(QObject::tr("Test1"));
    metaData().setGroup(QObject::tr("test"));
    m_input << "in 1" << "in 2" << "in 3";
    m_output << "out 1" << "out 2";
}


QList<QString> FCTestNode::getInputKeys() const
{
    return (m_input);
}


QList<QString> FCTestNode::getOutputKeys() const
{
    return (m_output);
}


FCAbstractNodeGraphicsItem *FCTestNode::createGraphicsItem()
{
    return (new FCTestNodeGraphicsItem(this));
}


FCDataPackage FCTestNode::output(const QString& ouputkey)
{
    return (FCDataPackage());
}
