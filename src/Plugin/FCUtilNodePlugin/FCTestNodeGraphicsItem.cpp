﻿#include "FCTestNodeGraphicsItem.h"
#include <QPainter>
#include "FCNodePalette.h"

FCTestNodeGraphicsItem::FCTestNodeGraphicsItem(FCTestNode *n, QGraphicsItem *p)
    : FCAbstractNodeGraphicsItem(n, p)
{
    resetLinkPoint();
}


void FCTestNodeGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->save();
    QPen pen(FCNodePalette::getGlobalEdgeColor());
    QRectF rec = boundingRect();

    pen.setWidth(1);
    if (isSelected()) {
        pen.setWidth(2);
        pen.setColor(pen.color().darker());
        rec.adjust(1, 1, -1, -1);
    }
    painter->setPen(pen);
    painter->fillRect(rec, FCNodePalette::getGlobalBackgroundColor());
    painter->drawRect(rec);
    //绘制点
    paintLinkPoints(painter, option, widget);
    painter->restore();
}


QRectF FCTestNodeGraphicsItem::boundingRect() const
{
    return (QRectF(0, 0, 50, 50));
}
