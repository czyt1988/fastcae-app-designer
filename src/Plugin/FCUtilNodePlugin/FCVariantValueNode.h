﻿#ifndef FCCONSTVALUENODE_H
#define FCCONSTVALUENODE_H
#include "FCUtilNodePluginGlobal.h"
#include "FCAbstractNode.h"
#include <QtCore/qglobal.h>
FC_IMPL_FORWARD_DECL(FCVariantValueNode)

/**
 * @brief 变量节点，可以产生一个常数
 */
class FCUTILNODEPLUGIN_API FCVariantValueNode : public FCAbstractNode
{
    FC_IMPL(FCVariantValueNode)
public:
    FCVariantValueNode();
    ~FCVariantValueNode();
    //获取所有输入的参数名
    virtual QList<QString> getInputKeys() const;

    //获取所有输出的参数名
    virtual QList<QString> getOutputKeys() const;

    //节点对应的item显示接口，所有node都需要提供一个供前端的显示接口
    virtual FCAbstractNodeGraphicsItem *createGraphicsItem();

    //输出参数
    virtual FCDataPackage output(const QString& ouputkey);

public:

    //字符串转换为variant
    static QVariant stringToVariant(const QString& var, const QString& typeName);
};

#endif // FCCONSTVALUENODE_H
