﻿#ifndef FCMETHODEDITORGRAPHICSVIEW_H
#define FCMETHODEDITORGRAPHICSVIEW_H
#include <QtCore/qglobal.h>
#include "FCNodeGraphicsView.h"
#include "FCProject.h"
#include "FCMethodEditorNodeFactory.h"
#include "FCMethodEditorGraphicsScene.h"
#include <QPointer>
#include "FCAbstractNodeWidget.h"
class GProject;

/**
 * @brief 用于节点显示的GraphicsView
 */
class FCMethodEditorGraphicsView : public FCNodeGraphicsView
{
    Q_OBJECT
public:
    FCMethodEditorGraphicsView(QWidget *parent = 0);
    FCMethodEditorGraphicsView(QGraphicsScene *scene, QWidget *parent = 0);
    void setProject(FCProject *project);
    void setNodeFactory(FCMethodEditorWorkFlow *factory);

signals:

    /**
     * @brief 选中了某个节点的设置窗口
     * @param w
     */
    void selectNodeItemChanged(FCAbstractNodeGraphicsItem *i);

protected:
    void dragEnterEvent(QDragEnterEvent *e) Q_DECL_OVERRIDE;
    void dragMoveEvent(QDragMoveEvent *e) Q_DECL_OVERRIDE;
    void dropEvent(QDropEvent *e) Q_DECL_OVERRIDE;

private slots:

    void onSceneSelectionChanged();

private:
    QPointer<FCProject> m_project;
    QPointer<FCMethodEditorWorkFlow> m_workflow;
};

#endif // GNODEGRAPHICSVIEW_H
