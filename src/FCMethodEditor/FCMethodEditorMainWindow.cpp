﻿#include "FCMethodEditorMainWindow.h"
#include "ui_FCMethodEditorMainWindow.h"
//插件相关
#include "FCMethodEditorPluginManager.h"
#include "FCPluginManager.h"
#include "FCAbstractPlugin.h"
#include "FCAbstractNodePlugin.h"
//对话框
#include "FCPluginManagerDialog.h"
//节点相关
#include "FCNodeMetaData.h"
#include "FCAbstractNodeFactory.h"
#include "FCAbstractNodeWidget.h"

FCMethodEditorMainWindow::FCMethodEditorMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::FCMethodEditorMainWindow)
{
    ui->setupUi(this);
    //首次调用此函数会加载插件，可放置在main函数中调用
    FCMethodEditorPluginManager::instance();
    m_workflow = new FCMethodEditorWorkFlow(this);
    initUI();
    initConnect();
}


FCMethodEditorMainWindow::~FCMethodEditorMainWindow()
{
    delete ui;
}


void FCMethodEditorMainWindow::setupNodeListWidget()
{
    FCMethodEditorPluginManager& plugin = FCMethodEditorPluginManager::instance();
    QList<FCAbstractNodeFactory *> factorys = plugin.getNodeFactorys();
}


void FCMethodEditorMainWindow::initUI()
{
    FCMethodEditorPluginManager& pluginmgr = FCMethodEditorPluginManager::instance();
    QList<FCAbstractNodeFactory *> factorys = pluginmgr.getNodeFactorys();
    //提取所有的元数据
    QList<FCNodeMetaData> nodeMetaDatas;

    for (FCAbstractNodeFactory *factory : factorys)
    {
        nodeMetaDatas += factory->getNodesMetaData();
        //注册工厂
        m_workflow->registFactory(factory);
        qDebug() << "registFactory:" << factory->getPrototypes();
    }
    //对所有元数据按group进行筛分
    QMap<QString, QList<FCNodeMetaData> > groupedNodeMetaDatas;

    for (const FCNodeMetaData& md : nodeMetaDatas)
    {
        groupedNodeMetaDatas[md.getGroup()].append(md);
    }
    //把数据写入toolbox
    ui->toolBox->addItems(groupedNodeMetaDatas);
    ui->graphicsView->setNodeFactory(m_workflow);
}


void FCMethodEditorMainWindow::initConnect()
{
    connect(ui->actionPluginManager, &QAction::triggered, this, &FCMethodEditorMainWindow::onActionPluginManagerTriggered);
    connect(ui->graphicsView,&FCMethodEditorGraphicsView::selectNodeItemChanged
            ,this,&FCMethodEditorMainWindow::onSelectNodeItemChanged);
}


void FCMethodEditorMainWindow::onActionPluginManagerTriggered(bool on)
{
    Q_UNUSED(on);
    FCPluginManagerDialog dlg(this);

    dlg.exec();
}

void FCMethodEditorMainWindow::onSelectNodeItemChanged(FCAbstractNodeGraphicsItem *i)
{
    if(nullptr == i){
        return;
    }
    FCAbstractNodeWidget* w = i->getNodeWidget();
    if(nullptr == w){
        return;
    }
    ui->dockWidgetSetting->setWidget(w);
}
