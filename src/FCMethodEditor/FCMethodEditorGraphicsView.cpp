﻿#include "FCMethodEditorGraphicsView.h"
#include <QDragEnterEvent>
#include <QDebug>
#include "FCAbstractNode.h"
#include "FCNodeMimeData.h"
#include "FCNodeMetaData.h"
#include "FCMimeTypeFormatDefine.h"
#include "FCMethodEditorGraphicsScene.h"

FCMethodEditorGraphicsView::FCMethodEditorGraphicsView(QWidget *parent)
    : FCNodeGraphicsView(parent)
{
    setAcceptDrops(true);
    FCMethodEditorGraphicsScene *sc = new FCMethodEditorGraphicsScene(this);

    setScene(sc);
    setRenderHint(QPainter::Antialiasing, false);
    //setDragMode(QGraphicsView::RubberBandDrag);
    setOptimizationFlags(QGraphicsView::DontSavePainterState);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    connect(sc, &FCMethodEditorGraphicsScene::selectionChanged, this, &FCMethodEditorGraphicsView::onSceneSelectionChanged);
}


FCMethodEditorGraphicsView::FCMethodEditorGraphicsView(QGraphicsScene *scene, QWidget *parent)
    : FCNodeGraphicsView(scene, parent)
{
    setAcceptDrops(true);
}


/**
 * @brief 设置工程
 * @param project
 */
void FCMethodEditorGraphicsView::setProject(FCProject *project)
{
    m_project = project;
}


void FCMethodEditorGraphicsView::setNodeFactory(FCMethodEditorWorkFlow *factory)
{
    m_workflow = factory;
}


void FCMethodEditorGraphicsView::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasFormat(MIME_NODE_META_DATA)) {
        //说明有节点的meta数据拖入
        e->acceptProposedAction();
    }else{
        e->ignore();
    }
}


void FCMethodEditorGraphicsView::dragMoveEvent(QDragMoveEvent *e)
{
    e->acceptProposedAction();
}


void FCMethodEditorGraphicsView::dropEvent(QDropEvent *e)
{
    if (e->mimeData()->hasFormat(MIME_NODE_META_DATA)) {
        //说明有节点的meta数据拖入
        const FCNodeMimeData *nodemime = qobject_cast<const FCNodeMimeData *>(e->mimeData());
        if (nullptr == nodemime) {
            qDebug()<<tr("drop have invalid mime data");
            return;
        }
        QGraphicsScene *sc = scene();
        FCNodeMetaData nodemeta = nodemime->getNodeMetaData();
        FCAbstractNode::Pointer node = m_workflow->createNode(nodemeta);
        if (nullptr == node) {
            qDebug()<<tr("can not create node,node prototype is:")<<node->getNodePrototype();
            return;
        }
        FCAbstractNodeGraphicsItem *item = node->createGraphicsItem();
        QPointF pos = mapToScene(e->pos());
        item->setPos(pos);
        sc->addItem(item);
    }
}


void FCMethodEditorGraphicsView::onSceneSelectionChanged()
{
    FCMethodEditorGraphicsScene *sc = qobject_cast<FCMethodEditorGraphicsScene *>(scene());

    if (nullptr == sc) {
        return;
    }
    FCAbstractNodeGraphicsItem *item = sc->getSelectedNodeGraphicsItem();

    if (item == nullptr) {
        return;
    }
    emit selectNodeItemChanged(item);
}
