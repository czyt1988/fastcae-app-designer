﻿#ifndef FCMETHODEDITORNODEFACTORY_H
#define FCMETHODEDITORNODEFACTORY_H

#include <QtCore/qglobal.h>
#include <QHash>
#include <QPointer>
#include "FCWorkFlow.h"
#include "FCAbstractNodeGraphicsItem.h"

/**
 * @brief 这个是总工厂，汇总了所有插件的工厂
 */
class FCMethodEditorWorkFlow : public FCWorkFlow
{
    Q_OBJECT
public:
    FCMethodEditorWorkFlow(QObject *p = nullptr);
};

#endif // FCMETHODEDITORNODEFACTORY_H
